package fr.epsi.model;

public class User {
	
	private String login;
	private String password;
	
	public String getlogin() {
	return login;
	}
	public void setLogin(final String plogin) {
		login =plogin;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(final String ppassword) {
		password=ppassword;
	}
	public User(final String plogin,final String ppassword) {
		login=plogin;
		password=ppassword;
	}
}
